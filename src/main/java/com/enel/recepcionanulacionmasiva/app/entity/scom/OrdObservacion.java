package com.enel.recepcionanulacionmasiva.app.entity.scom;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "ord_observacion")
public class OrdObservacion {

	@Id
	@Column(name = "id_observacion")
	private Long idObservacion;
	
	@Column(name = "id_orden")
	private Long idOrden;
	
	private String texto;
	
	@Column(name = "fecha_observacion")
	private Date fechaObservacion;
	
	@Column(name = "id_usuario")
	private Long idUsuario;
	
	@Column(name = "state_name")
	private String stateName;
	
	private String discriminator;
}
