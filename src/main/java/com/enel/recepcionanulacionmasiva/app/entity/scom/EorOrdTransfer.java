package com.enel.recepcionanulacionmasiva.app.entity.scom;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "eor_ord_transfer")
public class EorOrdTransfer {

	@Id
	@Column(name = "id_ord_transfer")
	private Long idOrdTransfer;
	
	@Column(name = "nro_orden_legacy")
	private String nroOrdenLegacy;
	
	@Column(name = "cod_tipo_orden_legacy")
	private String codTipoOrdenLegacy;
	
	@Column(name = "cod_tipo_orden_eorder")
	private String codTipoOrdenEOrder;
	
	@Column(name = "nro_anulaciones")
	private Short nroAnulaciones;
	
	@Column(name = "cod_estado_orden_ant")
	private Short codEstadoOrdenAnt;
	
	@Column(name = "cod_estado_orden")
	private Short codEstadoOrden;
	
	@Column(name = "origen")
	private String origen;
}
