package com.enel.recepcionanulacionmasiva.app.entity.s4j;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "cor_estado")
public class CorEstadoS4J {

	@Id
	private Long id;
	
	private char estado;
	private String descripcion;
}
