package com.enel.recepcionanulacionmasiva.app.entity.scom;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "fwk_auditevent")
public class FwkAuditevent {

	@Id
	private Long id;
	
	@Column(name = "fecha_ejecucion")
	private Date fechaEjecucion;
	
	private String objectref;
	
	@Column(name = "id_fk")
	private Long idFk;
	
	@Column(name = "id_user")
	private Long idUser;
	
	@Column(name = "specific_auditevent")
	private String specificAuditevent;
}
