package com.enel.recepcionanulacionmasiva.app.entity.scom;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "wkf_workflow")
public class WkfWorkflow {

	@Id
	private Long id;
	
	@Column(name = "id_state")
	private Long idState;
	
	@Column(name = "id_old_state")
	private Long idOldState;
}
