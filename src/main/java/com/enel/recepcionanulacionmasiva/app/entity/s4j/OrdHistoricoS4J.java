package com.enel.recepcionanulacionmasiva.app.entity.s4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "ord_historico")
public class OrdHistoricoS4J {

	@Id
	@Column(name = "id_auditevent")
	private Long idAuditevent;
	
	@Column(name = "estado_inicial")
	private String estadoInicial;
	
	@Column(name = "estado_final")
	private String estadoFinal;
	
	private String actividad;
	
	@Column(name = "id_buzon")
	private Long idBuzon;
	
}
