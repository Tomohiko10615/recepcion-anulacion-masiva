package com.enel.recepcionanulacionmasiva.app.entity.scom;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "usuario")
public class Usuario {

	@Id
	private Long id;
	
	private String username;
}
