package com.enel.recepcionanulacionmasiva.app.entity.scom;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "ord_orden")
public class OrdOrden {

	@Id
	private Long id;
	
	@Column(name = "nro_orden")
	private String nroOrden;
	
	@Column(name = "id_tipo_orden")
	private Long idTipoOrden;
	
	@Column(name = "id_workflow")
	private Long idWorkflow;
}
