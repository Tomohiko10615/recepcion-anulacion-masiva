package com.enel.recepcionanulacionmasiva.app.entity.s4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Data
@Table(name = "wkf_workflow")
public class WkfWorkflowS4J {

	@Id
	@Column(name = "id_workflow")
	private Long idWorkflow;
	
	@Column(name = "id_state")
	private Long idState;
	
	@Column(name = "id_old_state")
	private Long idOldState;
}
