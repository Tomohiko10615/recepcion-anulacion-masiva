package com.enel.recepcionanulacionmasiva.app;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.beans.factory.annotation.Autowired;

import com.enel.recepcionanulacionmasiva.app.dto.EstadoError;
import com.enel.recepcionanulacionmasiva.app.dto.Orden;
import com.enel.recepcionanulacionmasiva.app.dto.OrdenDetalles;
import com.enel.recepcionanulacionmasiva.app.dto.OrdenInfo;
import com.enel.recepcionanulacionmasiva.app.dto.WorkflowState;
import com.enel.recepcionanulacionmasiva.app.entity.s4j.FwkAuditeventS4J;
import com.enel.recepcionanulacionmasiva.app.entity.s4j.OrdHistoricoS4J;
import com.enel.recepcionanulacionmasiva.app.entity.s4j.OrdObservacionS4J;
import com.enel.recepcionanulacionmasiva.app.entity.scom.FwkAuditevent;
import com.enel.recepcionanulacionmasiva.app.entity.scom.OrdHistorico;
import com.enel.recepcionanulacionmasiva.app.entity.scom.OrdObservacion;
import com.enel.recepcionanulacionmasiva.app.service.s4j.CorEstadoS4JService;
import com.enel.recepcionanulacionmasiva.app.service.s4j.FwkAuditeventS4JService;
import com.enel.recepcionanulacionmasiva.app.service.s4j.OrdHistoricoS4JService;
import com.enel.recepcionanulacionmasiva.app.service.s4j.OrdObservacionS4JService;
import com.enel.recepcionanulacionmasiva.app.service.s4j.OrdOrdenS4JService;
import com.enel.recepcionanulacionmasiva.app.service.s4j.WkfWorkflowS4JService;
import com.enel.recepcionanulacionmasiva.app.service.scom.ComParametrosService;
import com.enel.recepcionanulacionmasiva.app.service.scom.CorEstadoService;
import com.enel.recepcionanulacionmasiva.app.service.scom.EorOrdTransferService;
import com.enel.recepcionanulacionmasiva.app.service.scom.FwkAuditeventService;
import com.enel.recepcionanulacionmasiva.app.service.scom.OrdHistoricoService;
import com.enel.recepcionanulacionmasiva.app.service.scom.OrdObservacionService;
import com.enel.recepcionanulacionmasiva.app.service.scom.OrdOrdenService;
import com.enel.recepcionanulacionmasiva.app.service.scom.UsuarioService;
import com.enel.recepcionanulacionmasiva.app.service.scom.UtilService;
import com.enel.recepcionanulacionmasiva.app.service.scom.WkfWorkflowService;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

@Data
@Slf4j
public class RecepcionAnulacionMasiva {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private ComParametrosService comParametrosService;
	
	@Autowired
	private EorOrdTransferService eorOrdTransferService;
	
	@Autowired
	private OrdOrdenService ordOrdenService;
	
	@Autowired
	private OrdOrdenS4JService ordOrdenS4JService;
	
	@Autowired
	private CorEstadoService corEstadoService;
	
	@Autowired
	private CorEstadoS4JService corEstadoS4JService;
	
	@Autowired
	private UtilService utilService;
	
	@Autowired
	private WkfWorkflowService wkfWorkflowService;
	
	@Autowired
	private FwkAuditeventService fwkAuditeventService;
	
	@Autowired
	private FwkAuditeventS4JService fwkAuditeventS4JService;
	
	@Autowired
	private OrdHistoricoService ordHistoricoService;
	
	@Autowired
	private OrdHistoricoS4JService ordHistoricoS4JService;
	
	@Autowired
	private OrdObservacionService ordObservacionService;
	
	@Autowired
	private OrdObservacionS4JService ordObservacionS4JService;
	
	@Autowired
	private WkfWorkflowS4JService wkfWorkflowS4JService;

	private String username;
	private String nroODT;
	private Date fechaActual;
	
	private Long idUsuario;
	private String fileEOrder;
	private String fileLegacy;
	private String prefijoORM;
	private String prefijoRecepAnul;
	private String pathSalidaEOrder;
	private Integer codEstPendAnul;
	private Integer codEstProc;
	private Integer codEstAnul;
	private Integer codEstAnulErr;
	private String errSyn;
	private String observErrSyn;
	
	private FileWriter archivoWriter;
	
	private List<Orden> ordenesScom = new ArrayList<>();
	private List<Orden> ordenesS4J= new ArrayList<>();
	
	private Integer registrosProcesados = 0;
	private Integer registrosRechazados = 0;
	private Integer registrosActualizados = 0;

	
	
	public boolean obtenerDatos() {
		idUsuario = usuarioService.obtenerIdUsuario(username);
		if (idUsuario == null) {
			log.error("No se pudo obtener Id usuario");
			return false;
		}
		
		log.info("Id usuario: {}", idUsuario);
		
		fileEOrder = comParametrosService.obtenerValorAlf("PATH_CNX", "CNX_EOR_OU");
		
		if (fileEOrder == null) {
			log.error("No se pudo obtener File EOrder");
			return false;
		}
		
		fileEOrder = fileEOrder.trim();
		
		log.info("File EOrder: {}", fileEOrder);
		
		fileLegacy = comParametrosService.obtenerValorAlf("PATH_CNX", "CNX_LGC_OU");

		if (fileLegacy == null) {
			log.error("No se pudo obtener File Legacy");
			return false;
		}
		
		fileLegacy = fileLegacy.trim();
		
		log.info("File Legacy: {}", fileLegacy);
		
		prefijoORM = comParametrosService.obtenerValorAlf("ARCHIVO_CNX", "FRAM_CNX03");
		if (prefijoORM == null) {
			log.error("No se pudo obtener Prefijo ORM");
			return false;
		}
		
		prefijoORM = prefijoORM.trim();
		
		log.info("Prefijo ORM: {}", prefijoORM);
		
		prefijoRecepAnul = comParametrosService.obtenerValorAlf("ARCHIVO_CNX", "FRAM_CNX05");
		
		if (prefijoRecepAnul == null) {
			log.error("No se pudo obtener Prefijo Recepcion Anulacion");
			return false;
		}
		
		prefijoRecepAnul = prefijoRecepAnul.trim();
		
		log.info("Prefijo Recepcion Anulacion: {}", prefijoRecepAnul);
		
		pathSalidaEOrder = comParametrosService.obtenerValorAlf("PATH_CNX", "CNX_PROCES");
		
		if (pathSalidaEOrder == null) {
			log.error("No se pudo obtener Path Salida EOrder");
			return false;
		}
		
		pathSalidaEOrder = pathSalidaEOrder.trim();
		
		log.info("Path Salida EOrder: {}", pathSalidaEOrder);
		
		codEstPendAnul = comParametrosService.obtenerValorNum("PANUL");
		
		if (codEstPendAnul == null) {
			log.error("No se pudo obtener Codigo Estado Pendiente Anulacion");
			return false;
		}
		
		log.info("Codigo Estado Pendiente Anulacion: {}", codEstPendAnul);
		
		codEstProc = comParametrosService.obtenerValorNum("PROCE");
		
		if (codEstProc == null) {
			log.error("No se pudo obtener Codigo Estado Proceso");
			return false;
		}
		
		log.info("Codigo Estado Proceso: {}", codEstProc);
		
		codEstAnul = comParametrosService.obtenerValorNum("ANULA");
		
		if (codEstAnul == null) {
			log.error("No se pudo obtener Codigo Estado Anulacion");
			return false;
		}
		
		log.info("Codigo Estado Anulacion: {}", codEstAnul);
		
		codEstAnulErr = comParametrosService.obtenerValorNum("ANUER");
		
		if (codEstAnulErr == null) {
			log.error("No se pudo obtener Codigo Estado Anulacion Error");
			return false;
		}
		
		log.info("Codigo Estado Anulacion Error: {}", codEstAnulErr);
		
		EstadoError estadoError = comParametrosService.obtenerEstadoError();
		
		if (estadoError == null) {
			log.error("No se pudo obtener Estado Error");
			return false;
		} else {
			errSyn = estadoError.getErrSyn();
			log.info("Codigo Estado Error: {}", errSyn);
			observErrSyn = estadoError.getDescripcion();
			log.info("Codigo Estado Error Observacion: {}", observErrSyn);
		}
		
		return true;
	}

	public boolean crearArchivo() throws IOException {
		
		log.info("Creando archivo de salida...");
		
		String pathArchivo = fileLegacy
				+ "EOR_CNX_RECEP_ANUL_MASIVA_ORMyFACT_ERRORES_"
				+ nroODT.trim();
		
		File archivo = new File(pathArchivo);
		
		archivoWriter = new FileWriter(archivo);
		
		log.info("Archivo creado con exito");
		
		return true;
	}

	public boolean ejecutarProceso() throws IOException {
		
		log.info("Iniciando proceso...");
		
		obtenerOrdenes();

		if (!ordenesScom.isEmpty()) {
			procesarOrdenesScom(ordenesScom);
			
		} else {
			log.info("No existen ordenes del SCOM para procesar");
		}
		
		if (!ordenesS4J.isEmpty()) {
			procesarOrdenesS4J(ordenesS4J);
		} else {
			log.info("No existen ordenes del S4J para procesar");
		}
		
		return true;
	}

	private void procesarOrdenesS4J(List<Orden> ordenes) throws IOException {
		
		for (Orden orden : ordenes) {
			registrosProcesados++;
			
			log.info("Validando existencia de orden en estado creada...");
			
			log.info(orden.getNroOrdenLegacy());
			log.info(orden.getCodTipoOrdenLegacy());
			
			boolean existeOrdenCreada = 
					ordOrdenS4JService.validarExistencia(
							orden.getNroOrdenLegacy(), orden.getCodTipoOrdenLegacy());
			
			if (existeOrdenCreada) {
				if (!verificaOrmDesmantelamiento(orden)) {
					log.info("La orden existe y no se ha verificado el desmantelamiento");
					log.info("Saltando a la siguiente orden...");
					continue;
				}
				
				log.info("Procesando orden...");
				log.info("Obteniendo datos de anulación...");
				
				OrdenInfo ordenInfo = ordOrdenS4JService.obtenerOrdenInfo(orden.getNroOrdenLegacy(), orden.getCodTipoOrdenLegacy());
				
				Long idAuditevent = fwkAuditeventS4JService.obtenerIdAuditevent();
				
				log.info("Anulando la orden en la tabla de transferencia...");
				
				eorOrdTransferService.anularOrden(codEstAnul, orden.getIdOrdTransfer());
				
				log.info("Insertando registro en la tabla FWK_AUDITEVENT...");
				
				FwkAuditeventS4J newFwkAuditeventS4J = new FwkAuditeventS4J();
					
				newFwkAuditeventS4J.setId(idAuditevent);
				newFwkAuditeventS4J.setFechaEjecucion(utilService.obtenerFechaSistema());
				newFwkAuditeventS4J.setObjectref("com.synapsis.synergia.orm.domain.OrdenMantenimiento");
				newFwkAuditeventS4J.setIdFk(ordenInfo.getIdOrden());
				newFwkAuditeventS4J.setIdUser(idUsuario);
				newFwkAuditeventS4J.setSpecificAuditevent("ORD");
			
				newFwkAuditeventS4J = fwkAuditeventS4JService.save(newFwkAuditeventS4J);
				
				if (newFwkAuditeventS4J == null) {
					log.error("Error Insert FWK_AUDITEVENT...");
					System.exit(1);
				}
				
				log.info("Insertando registro en la tabla ORD_HISTORICO...");
				
				OrdHistoricoS4J newOrdHistoricoS4J = new OrdHistoricoS4J();
				
				newOrdHistoricoS4J.setIdAuditevent(idAuditevent);
				newOrdHistoricoS4J.setEstadoInicial("Orden Creada");
				newOrdHistoricoS4J.setEstadoFinal("Orden Anulada");
				newOrdHistoricoS4J.setActividad("Anula Orden");
				newOrdHistoricoS4J.setIdBuzon(ordenInfo.getIdBuzon());
				
				newOrdHistoricoS4J = ordHistoricoS4JService.save(newOrdHistoricoS4J);
				
				if (newOrdHistoricoS4J == null) {
					log.error("Error Insert ORD_HISTORICO...");
					System.exit(1);
				}
				
				log.info("Actualizando registro de tabla ORD_ORDEN...");
				
				ordOrdenS4JService.actualizarOrden(orden.getNroOrdenLegacy(), ordenInfo.getIdOrden(), ordenInfo.getIdTipoOrden());
	
				log.info("Actualizando registro de tabla WKF_WORKFLOW...");
				
				wkfWorkflowS4JService.actualizarWorkflow(ordenInfo.getIdWorkflowOrd());
				
				log.info("Insertando un registro en la tabla de Observaciones...");
				
				Long idObservacion = ordObservacionS4JService.obtenerIdObservacion();
				
				OrdObservacionS4J newOrdObservacionS4J = new OrdObservacionS4J();
				
				newOrdObservacionS4J.setIdObservacion(idObservacion);
				newOrdObservacionS4J.setIdOrden(ordenInfo.getIdOrden());
				newOrdObservacionS4J.setTexto("Anulación masiva desde interface con eOrder");
				newOrdObservacionS4J.setFechaObservacion(utilService.obtenerFechaSistema());
				newOrdObservacionS4J.setStateName("Anulada");
				newOrdObservacionS4J.setDiscriminator("ObeservacionOrden");
				newOrdObservacionS4J.setIdUsuario(idUsuario);
				
				newOrdObservacionS4J = ordObservacionS4JService.save(newOrdObservacionS4J);
				
				if (newOrdObservacionS4J == null) {
					log.error("Error Insert OBSERVACIONES...");
					System.exit(1);
				}
				
				registrosActualizados++;
				archivoWriter.write(orden.getNroOrdenLegacy() + "\tANULADA\n");
				
			} else {
				
				archivoWriter.write(orden.getNroOrdenLegacy() + "\tNO ANULADA \t No se puede anular, orden no se encuentra en estado 'Creada'\n");
				eorOrdTransferService.actualizarConError(
						codEstAnulErr, errSyn, observErrSyn, orden.getIdOrdTransfer());
				eorOrdTransferService.actualizarDetalle(
						errSyn, orden.getIdOrdTransfer(), orden.getNroAnulaciones());
				registrosRechazados++;
			}
		}
	}

	private void procesarOrdenesScom(List<Orden> ordenes) throws IOException {
		
		for (Orden orden : ordenes) {
			registrosProcesados++;
			
			log.info("Validando existencia de orden en estado creada...");
			
			log.info(orden.getNroOrdenLegacy());
			log.info(orden.getCodTipoOrdenLegacy());
			
			boolean existeOrdenCreada = 
					ordOrdenService.validarExistencia(
							orden.getNroOrdenLegacy(), orden.getCodTipoOrdenLegacy());
			
			if (existeOrdenCreada) {
				
				log.info("Procesando orden...");
				log.info("Obteniendo datos de anulación...");
				
				OrdenInfo ordenInfo = ordOrdenService.obtenerOrdenInfo(orden.getNroOrdenLegacy(), orden.getCodTipoOrdenLegacy());
				
				Long idAuditevent = utilService.obtenerIdAuditevent();
				
				log.info("Anulando la orden en la tabla de transferencia...");
				
				eorOrdTransferService.anularOrden(codEstAnul, orden.getIdOrdTransfer());
				
				log.info("Insertando registro en la tabla FWK_AUDITEVENT...");
				
				FwkAuditevent newFwkAuditevent = new FwkAuditevent();
					
				newFwkAuditevent.setId(idAuditevent);
				newFwkAuditevent.setFechaEjecucion(utilService.obtenerFechaSistema());
				newFwkAuditevent.setObjectref("com.synapsis.synergia.orm.domain.OrdenMantenimiento");
				newFwkAuditevent.setIdFk(ordenInfo.getIdOrden());
				newFwkAuditevent.setIdUser(idUsuario);
				newFwkAuditevent.setSpecificAuditevent("ORD");
			
				newFwkAuditevent = fwkAuditeventService.save(newFwkAuditevent);
				
				if (newFwkAuditevent == null) {
					log.error("Error Insert FWK_AUDITEVENT...");
					System.exit(1);
				}
				
				log.info("Insertando registro en la tabla ORD_HISTORICO...");
				
				OrdHistorico newOrdHistorico = new OrdHistorico();
				
				newOrdHistorico.setIdAuditevent(idAuditevent);
				newOrdHistorico.setEstadoInicial("Orden Creada");
				newOrdHistorico.setEstadoFinal("Orden Anulada");
				newOrdHistorico.setActividad("Anula Orden");
				newOrdHistorico.setIdBuzon(ordenInfo.getIdBuzon());
				
				newOrdHistorico = ordHistoricoService.save(newOrdHistorico);
				
				if (newOrdHistorico == null) {
					log.error("Error Insert ORD_HISTORICO...");
					System.exit(1);
				}
				
				log.info("Actualizando registro de tabla ORD_ORDEN...");
				
				ordOrdenService.actualizarOrden(orden.getNroOrdenLegacy(), ordenInfo.getIdOrden(), ordenInfo.getIdTipoOrden());
	
				log.info("Actualizando registro de tabla WKF_WORKFLOW...");
				
				wkfWorkflowService.actualizarWorkflow(ordenInfo.getIdWorkflowOrd());
				
				log.info("Insertando un registro en la tabla de Observaciones...");
				
				Long idObservacion = utilService.obtenerIdObservacion();
				
				OrdObservacion newOrdObservacion = new OrdObservacion();
				
				newOrdObservacion.setIdObservacion(idObservacion);
				newOrdObservacion.setIdOrden(ordenInfo.getIdOrden());
				newOrdObservacion.setTexto("Anulación masiva desde interface con eOrder");
				newOrdObservacion.setFechaObservacion(utilService.obtenerFechaSistema());
				newOrdObservacion.setStateName("Anulada");
				newOrdObservacion.setDiscriminator("ObeservacionOrden");
				newOrdObservacion.setIdUsuario(idUsuario);
				
				newOrdObservacion = ordObservacionService.save(newOrdObservacion);
				
				if (newOrdObservacion == null) {
					log.error("Error Insert OBSERVACIONES...");
					System.exit(1);
				}
				
				registrosActualizados++;
				archivoWriter.write(orden.getNroOrdenLegacy() + "\tANULADA\n");
				
			} else {
				archivoWriter.write(orden.getNroOrdenLegacy() + "\tNO ANULADA \t No se puede anular, orden no se encuentra en estado 'Creada'\n");
				eorOrdTransferService.actualizarConError(
						codEstAnulErr, errSyn, observErrSyn, orden.getIdOrdTransfer());
				eorOrdTransferService.actualizarDetalle(
						errSyn, orden.getIdOrdTransfer(), orden.getNroAnulaciones());
				registrosRechazados++;
			}
		}
	}

	private boolean verificaOrmDesmantelamiento(Orden orden) {
		
		log.info("Verificando desmantelamiento...");
		
		String observacion;
		
		OrdenDetalles ordenDetalles = ordOrdenS4JService.obtenerOrdenDetalles(orden.getNroOrdenLegacy());
		
		String detalles = comParametrosService.obtenerDetalles();
		
		if (ordenDetalles.getDetalles().equals(detalles)) {
			WorkflowState  workflowState 
				= ordOrdenS4JService.obtenerWorkflowState(ordenDetalles.getIdOrden());
			
			String estadoFinal;
			try {
				estadoFinal = corEstadoS4JService.obtenerDescripcion(workflowState.getIdState());
				if (estadoFinal == null) {
					throw new NoResultException();
				}
			} catch (Exception e) {
				observacion = "Error: No se puede anular, Servicio en Estado " + workflowState.getIdState();
				
				eorOrdTransferService.actualizarConError(
						codEstAnulErr, "99", observacion, orden.getIdOrdTransfer());
				
				return false;
			}
			
			try {
				wkfWorkflowS4JService.actualizarEstadoWorkflow(estadoFinal, workflowState.getIdWorkflow());
			} catch (Exception e) {
				observacion = "Error: Error No se pudo actualizar Servicio Electrico";
				eorOrdTransferService.actualizarConError(
						codEstAnulErr, "99", observacion, orden.getIdOrdTransfer());
				return false;
			}
		}
		
		log.info("{} != {} Se verificó el desmantelamiento. Seguir con el proceso normal.", ordenDetalles.getDetalles(), detalles);
		
		return true;
	}

	private void obtenerOrdenes() {
		
		log.info("Actualizando EOR_ORD_TRANSFER...");
		
		eorOrdTransferService.actualizar(codEstProc, codEstPendAnul);
		
		log.info("{} {}", codEstPendAnul, codEstProc);
		
		List<Orden> ordenes = eorOrdTransferService.obtenerOrdenes(codEstPendAnul, codEstProc);
		
		for (Orden orden : ordenes) {
			if (orden.getOrigen().equals("SCOM")) {
				ordenesScom.add(orden);
			} else {
				ordenesS4J.add(orden);
			}
			log.info(orden.getOrigen());
		}
		
	}
}
