package com.enel.recepcionanulacionmasiva.app;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RecepcionAnulacionMasivaConfig {

	@Bean(name = "app")
	RecepcionAnulacionMasiva registrarrRecepcionAnulacionMasiva() {
		return new RecepcionAnulacionMasiva();
	}
}
