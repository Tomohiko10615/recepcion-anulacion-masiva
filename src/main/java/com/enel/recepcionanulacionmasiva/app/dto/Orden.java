package com.enel.recepcionanulacionmasiva.app.dto;

public interface Orden {

	Long getIdOrdTransfer();
	String getNroOrdenLegacy();
	String getCodTipoOrdenLegacy();
	String getCodTipoOrdenEOrder();
	Short getNroAnulaciones();
	String getOrigen();
}
