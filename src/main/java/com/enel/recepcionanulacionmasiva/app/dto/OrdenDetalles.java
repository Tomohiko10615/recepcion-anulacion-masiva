package com.enel.recepcionanulacionmasiva.app.dto;

public interface OrdenDetalles {

	Long getIdOrden();
	String getDetalles();
}
