package com.enel.recepcionanulacionmasiva.app.dto;

public interface OrdenInfo {

	Long getIdOrden();
	Long getIdBuzon();
	Long getIdWorkflowOrd();
	Long getIdTipoOrden();
	
}
