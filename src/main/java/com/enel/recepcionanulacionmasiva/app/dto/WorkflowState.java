package com.enel.recepcionanulacionmasiva.app.dto;

public interface WorkflowState {

	Long getIdWorkflow();
	Long getIdState();
}
