package com.enel.recepcionanulacionmasiva.app.dto;

public interface EstadoError {

	String getErrSyn();
	String getDescripcion();
}
