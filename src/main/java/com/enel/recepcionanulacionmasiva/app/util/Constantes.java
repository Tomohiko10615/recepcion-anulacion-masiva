package com.enel.recepcionanulacionmasiva.app.util;

public class Constantes {

	private Constantes() {
		super();
	}
	public static final String ESQUEMA = "SCHSCOM";
	public static final String ESQUEMADOT = "";
	public static final String KEY_PATH_IN = "MED_REPORTES_IN";
	public static final String KEY_PATH_OUT = "MED_REPORTES";
	public static final String TYPE_UBICACION_TRA = "com.synapsis.synergia.med.domain.componente.AlmacenTrazabilidad";
	public static final Long ID_EST_COMPONENTE = 16L;
	public static final String ERROR_ID_MMMEDIDOR = "\n\tError al Obtener id nuevo registro de MED_MEDIDA_MEDIDOR";
	public static final String ERROR_INS_MMMEDIDOR = "\n\tError al Insertar en MED_MEDIDA_MEDIDOR";
	public static final String TYPE_COMPONENTE = "com.synapsis.synergia.med.domain.componente.Medidor";
	
	
}
