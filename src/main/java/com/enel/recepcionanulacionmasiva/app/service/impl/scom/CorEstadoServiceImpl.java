package com.enel.recepcionanulacionmasiva.app.service.impl.scom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionanulacionmasiva.app.repository.scom.CorEstadoRepository;
import com.enel.recepcionanulacionmasiva.app.service.scom.CorEstadoService;

@Service
public class CorEstadoServiceImpl implements CorEstadoService {

	@Autowired
	private CorEstadoRepository corEstadoRepository;
	
	@Override
	public String obtenerDescripcion(Long idState) {
		return corEstadoRepository.obtenerDescripcion(idState);
	}

}
