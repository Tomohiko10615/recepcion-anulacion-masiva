package com.enel.recepcionanulacionmasiva.app.service.s4j;

import com.enel.recepcionanulacionmasiva.app.entity.s4j.OrdObservacionS4J;

public interface OrdObservacionS4JService {

	OrdObservacionS4J save(OrdObservacionS4J newOrdObservacionS4J);

	Long obtenerIdObservacion();

}
