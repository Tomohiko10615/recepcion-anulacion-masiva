package com.enel.recepcionanulacionmasiva.app.service.scom;

import com.enel.recepcionanulacionmasiva.app.entity.scom.OrdObservacion;

public interface OrdObservacionService {

	OrdObservacion save(OrdObservacion newOrdObservacion);

}
