package com.enel.recepcionanulacionmasiva.app.service.scom;

import java.util.List;

import com.enel.recepcionanulacionmasiva.app.dto.Orden;

public interface EorOrdTransferService {

	void actualizar(Integer codEstProc, Integer codEstPendAnul);

	List<Orden> obtenerOrdenes(Integer codEstPendAnul, Integer codEstProc);

	void actualizarConError(Integer codEstAnulErr, String errSyn, String observacion, Long idOrdTransfer);

	void anularOrden(Integer codEstAnul, Long idOrdTransfer);

	void actualizarDetalle(String errSyn, Long idOrdTransfer, Short nroAnulaciones);

}
