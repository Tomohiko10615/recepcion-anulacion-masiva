package com.enel.recepcionanulacionmasiva.app.service.impl.s4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionanulacionmasiva.app.repository.s4j.WkfWorkflowS4JRepository;
import com.enel.recepcionanulacionmasiva.app.service.s4j.WkfWorkflowS4JService;

@Service
public class WkfWorkflowS4JServiceImpl implements WkfWorkflowS4JService {

	@Autowired
	private WkfWorkflowS4JRepository wkfWorkflowS4JRepository;
	
	@Override
	public void actualizarEstadoWorkflow(String estadoFinal, Long idWorkflow) {
		wkfWorkflowS4JRepository.actualizarEstadoWorkflow(estadoFinal, idWorkflow);
	}

	@Override
	public void actualizarWorkflow(Long idWorkflowOrd) {
		wkfWorkflowS4JRepository.actualizarWorkflow(idWorkflowOrd);
	}

}
