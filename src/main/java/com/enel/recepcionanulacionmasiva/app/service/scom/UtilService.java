package com.enel.recepcionanulacionmasiva.app.service.scom;

import java.util.Date;

public interface UtilService {

	Date obtenerFechaSistema();

	Long obtenerIdObservacion();

	Long obtenerIdAuditevent();

}
