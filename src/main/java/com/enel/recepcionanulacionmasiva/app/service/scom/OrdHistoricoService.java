package com.enel.recepcionanulacionmasiva.app.service.scom;

import com.enel.recepcionanulacionmasiva.app.entity.scom.OrdHistorico;

public interface OrdHistoricoService {

	OrdHistorico save(OrdHistorico newOrdHistorico);

}
