package com.enel.recepcionanulacionmasiva.app.service.impl.s4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionanulacionmasiva.app.entity.s4j.OrdObservacionS4J;
import com.enel.recepcionanulacionmasiva.app.repository.s4j.OrdObservacionS4JRepository;
import com.enel.recepcionanulacionmasiva.app.service.s4j.OrdObservacionS4JService;

@Service
public class OrdObservacionS4JServiceImpl implements OrdObservacionS4JService {

	@Autowired
	private OrdObservacionS4JRepository ordObservacionS4JRepository;
	
	@Override
	public OrdObservacionS4J save(OrdObservacionS4J newOrdObservacionS4J) {
		return ordObservacionS4JRepository.save(newOrdObservacionS4J);
	}

	@Override
	public Long obtenerIdObservacion() {
		return ordObservacionS4JRepository.obtenerIdObservacion();
	}

}
