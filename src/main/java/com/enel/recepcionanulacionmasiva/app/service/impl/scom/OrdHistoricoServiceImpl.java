package com.enel.recepcionanulacionmasiva.app.service.impl.scom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionanulacionmasiva.app.entity.scom.OrdHistorico;
import com.enel.recepcionanulacionmasiva.app.repository.scom.OrdHistoricoRepository;
import com.enel.recepcionanulacionmasiva.app.service.scom.OrdHistoricoService;

@Service
public class OrdHistoricoServiceImpl implements OrdHistoricoService {

	@Autowired
	private OrdHistoricoRepository ordHistoricoRepository;
	
	@Override
	public OrdHistorico save(OrdHistorico newOrdHistorico) {
		return ordHistoricoRepository.save(newOrdHistorico);
	}

}
