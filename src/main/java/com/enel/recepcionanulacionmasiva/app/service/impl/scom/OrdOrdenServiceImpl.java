package com.enel.recepcionanulacionmasiva.app.service.impl.scom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionanulacionmasiva.app.dto.OrdenDetalles;
import com.enel.recepcionanulacionmasiva.app.dto.OrdenInfo;
import com.enel.recepcionanulacionmasiva.app.dto.WorkflowState;
import com.enel.recepcionanulacionmasiva.app.repository.scom.OrdOrdenRepository;
import com.enel.recepcionanulacionmasiva.app.service.scom.OrdOrdenService;

@Service
public class OrdOrdenServiceImpl implements OrdOrdenService {

	@Autowired
	private OrdOrdenRepository ordOrdenRepository;
	
	@Override
	public boolean validarExistencia(String nroOrdenLegacy, String codTipoOrdenLegacy) {
		return ordOrdenRepository.contarOrdenes(nroOrdenLegacy, codTipoOrdenLegacy) > 0;
	}

	@Override
	public OrdenDetalles obtenerOrdenDetalles(String nroOrdenLegacy) {
		return ordOrdenRepository.obtenerOrdenDetalles(nroOrdenLegacy);
	}

	@Override
	public WorkflowState obtenerWorkflowState(Long idOrden) {
		return ordOrdenRepository.obtenerWorkflowState(idOrden);
	}

	@Override
	public OrdenInfo obtenerOrdenInfo(String nroOrdenLegacy, String codTipoOrdenLegacy) {
		return ordOrdenRepository.obtenerOrdenInfo(nroOrdenLegacy, codTipoOrdenLegacy);
			
	}

	@Override
	public void actualizarOrden(String nroOrdenLegacy, Long idOrden, Long idTipoOrden) {
		ordOrdenRepository.actualizarOrden(nroOrdenLegacy, idOrden, idTipoOrden);
	}

}
