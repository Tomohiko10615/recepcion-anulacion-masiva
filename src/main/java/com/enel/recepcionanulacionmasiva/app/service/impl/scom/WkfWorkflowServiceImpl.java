package com.enel.recepcionanulacionmasiva.app.service.impl.scom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionanulacionmasiva.app.repository.scom.WkfWorkflowRepository;
import com.enel.recepcionanulacionmasiva.app.service.scom.WkfWorkflowService;

@Service
public class WkfWorkflowServiceImpl implements WkfWorkflowService {

	@Autowired
	private WkfWorkflowRepository wkfWorkflowRepository;
	
	@Override
	public void actualizarEstadoWorkflow(String estadoFinal, Long idWorkflow) {
		wkfWorkflowRepository.actualizarEstadoWorkflow(estadoFinal, idWorkflow);
	}

	@Override
	public void actualizarWorkflow(Long idWorkflowOrd) {
		wkfWorkflowRepository.actualizarWorkflow(idWorkflowOrd);
	}

}
