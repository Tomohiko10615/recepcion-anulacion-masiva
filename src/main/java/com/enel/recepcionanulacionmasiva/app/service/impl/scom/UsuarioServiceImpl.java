package com.enel.recepcionanulacionmasiva.app.service.impl.scom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionanulacionmasiva.app.repository.scom.UsuarioRepository;
import com.enel.recepcionanulacionmasiva.app.service.scom.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Override
	public Long obtenerIdUsuario(String username) {
		return usuarioRepository.obtenerUsuarioId(username);
	}

}
