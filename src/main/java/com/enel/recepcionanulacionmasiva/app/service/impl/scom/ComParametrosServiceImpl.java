package com.enel.recepcionanulacionmasiva.app.service.impl.scom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionanulacionmasiva.app.dto.EstadoError;
import com.enel.recepcionanulacionmasiva.app.repository.scom.ComParametrosRepository;
import com.enel.recepcionanulacionmasiva.app.service.scom.ComParametrosService;

@Service
public class ComParametrosServiceImpl implements ComParametrosService {

	@Autowired
	private ComParametrosRepository comParametrosRepository;
	
	@Override
	public String obtenerValorAlf(String entidad, String codigo) {
		return comParametrosRepository.obtenerValorAlf(entidad, codigo);
	}

	@Override
	public Integer obtenerValorNum(String codigo) {
		return comParametrosRepository.obtenerValorNum(codigo);
	}

	@Override
	public EstadoError obtenerEstadoError() {
		return comParametrosRepository.obtenerEstadoError();
	}

	@Override
	public String obtenerDetalles() {
		return comParametrosRepository.obtenerDetalles();
	}

}
