package com.enel.recepcionanulacionmasiva.app.service.s4j;

public interface CorEstadoS4JService {

	String obtenerDescripcion(Long idState);

}
