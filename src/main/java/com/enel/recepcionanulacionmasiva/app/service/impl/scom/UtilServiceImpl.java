package com.enel.recepcionanulacionmasiva.app.service.impl.scom;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionanulacionmasiva.app.repository.scom.UtilRepository;
import com.enel.recepcionanulacionmasiva.app.service.scom.UtilService;

@Service
public class UtilServiceImpl implements UtilService {

	@Autowired
	private UtilRepository utilRepository;

	@Override
	public Date obtenerFechaSistema() {
		return utilRepository.obtenerFechaSistema();
	}

	@Override
	public Long obtenerIdObservacion() {
		return utilRepository.obtenerIdObservacion();
	}

	@Override
	public Long obtenerIdAuditevent() {
		return utilRepository.obtenerIdAuditevent();
	}

}
