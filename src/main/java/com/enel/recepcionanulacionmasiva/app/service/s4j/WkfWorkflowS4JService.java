package com.enel.recepcionanulacionmasiva.app.service.s4j;

public interface WkfWorkflowS4JService {

	void actualizarEstadoWorkflow(String estadoFinal, Long idWorkflow);

	void actualizarWorkflow(Long idWorkflowOrd);

}
