package com.enel.recepcionanulacionmasiva.app.service.impl.s4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionanulacionmasiva.app.entity.s4j.FwkAuditeventS4J;
import com.enel.recepcionanulacionmasiva.app.repository.s4j.FwkAuditeventS4JRepository;
import com.enel.recepcionanulacionmasiva.app.service.s4j.FwkAuditeventS4JService;

@Service
public class FwkAuditeventS4JServiceImpl implements FwkAuditeventS4JService {

	@Autowired
	FwkAuditeventS4JRepository fwkAuditeventS4JRepository;
	
	@Override
	public FwkAuditeventS4J save(FwkAuditeventS4J newFwkAuditeventS4J) {
		return fwkAuditeventS4JRepository.save(newFwkAuditeventS4J);
	}

	@Override
	public Long obtenerIdAuditevent() {
		return fwkAuditeventS4JRepository.obtenerIdAuditevent();
	}

}
