package com.enel.recepcionanulacionmasiva.app.service.impl.s4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionanulacionmasiva.app.repository.s4j.CorEstadoS4JRepository;
import com.enel.recepcionanulacionmasiva.app.service.s4j.CorEstadoS4JService;

@Service
public class CorEstadoS4JServiceImpl implements CorEstadoS4JService {

	@Autowired
	private CorEstadoS4JRepository corEstadoS4JRepository;
	
	@Override
	public String obtenerDescripcion(Long idState) {
		return corEstadoS4JRepository.obtenerDescripcion(idState);
	}

}
