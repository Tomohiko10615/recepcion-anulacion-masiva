package com.enel.recepcionanulacionmasiva.app.service.impl.s4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionanulacionmasiva.app.dto.OrdenDetalles;
import com.enel.recepcionanulacionmasiva.app.dto.OrdenInfo;
import com.enel.recepcionanulacionmasiva.app.dto.WorkflowState;
import com.enel.recepcionanulacionmasiva.app.repository.s4j.OrdOrdenS4JRepository;
import com.enel.recepcionanulacionmasiva.app.service.s4j.OrdOrdenS4JService;

@Service
public class OrdOrdenS4JServiceImpl implements OrdOrdenS4JService {

	@Autowired
	private OrdOrdenS4JRepository ordOrdenS4JRepository;
	
	@Override
	public boolean validarExistencia(String nroOrdenLegacy, String codTipoOrdenLegacy) {
		return ordOrdenS4JRepository.contarOrdenes(nroOrdenLegacy, codTipoOrdenLegacy) > 0;
	}

	@Override
	public OrdenDetalles obtenerOrdenDetalles(String nroOrdenLegacy) {
		return ordOrdenS4JRepository.obtenerOrdenDetalles(nroOrdenLegacy);
	}

	@Override
	public WorkflowState obtenerWorkflowState(Long idOrden) {
		return ordOrdenS4JRepository.obtenerWorkflowState(idOrden);
	}

	@Override
	public OrdenInfo obtenerOrdenInfo(String nroOrdenLegacy, String codTipoOrdenLegacy) {
		return ordOrdenS4JRepository.obtenerOrdenInfo(nroOrdenLegacy, codTipoOrdenLegacy);
			
	}

	@Override
	public void actualizarOrden(String nroOrdenLegacy, Long idOrden, Long idTipoOrden) {
		ordOrdenS4JRepository.actualizarOrden(nroOrdenLegacy, idOrden, idTipoOrden);
	}

}
