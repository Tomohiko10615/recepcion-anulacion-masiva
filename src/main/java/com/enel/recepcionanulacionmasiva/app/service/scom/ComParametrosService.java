package com.enel.recepcionanulacionmasiva.app.service.scom;

import com.enel.recepcionanulacionmasiva.app.dto.EstadoError;

public interface ComParametrosService {

	String obtenerValorAlf(String entidad, String codigo);

	Integer obtenerValorNum(String codigo);

	EstadoError obtenerEstadoError();

	String obtenerDetalles();

}
