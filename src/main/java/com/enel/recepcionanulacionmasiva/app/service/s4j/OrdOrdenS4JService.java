package com.enel.recepcionanulacionmasiva.app.service.s4j;

import com.enel.recepcionanulacionmasiva.app.dto.OrdenDetalles;
import com.enel.recepcionanulacionmasiva.app.dto.OrdenInfo;
import com.enel.recepcionanulacionmasiva.app.dto.WorkflowState;

public interface OrdOrdenS4JService {

	boolean validarExistencia(String nroOrdenLegacy, String codTipoOrdenLegacy);

	OrdenDetalles obtenerOrdenDetalles(String nroOrdenLegacy);

	WorkflowState obtenerWorkflowState(Long idOrden);

	OrdenInfo obtenerOrdenInfo(String nroOrdenLegacy, String codTipoOrdenLegacy);

	void actualizarOrden(String nroOrdenLegacy, Long idOrden, Long idTipoOrden);

}
