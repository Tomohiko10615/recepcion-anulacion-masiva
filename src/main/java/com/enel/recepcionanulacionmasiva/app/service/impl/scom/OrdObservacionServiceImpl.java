package com.enel.recepcionanulacionmasiva.app.service.impl.scom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionanulacionmasiva.app.entity.scom.OrdObservacion;
import com.enel.recepcionanulacionmasiva.app.repository.scom.OrdObservacionRepository;
import com.enel.recepcionanulacionmasiva.app.service.scom.OrdObservacionService;

@Service
public class OrdObservacionServiceImpl implements OrdObservacionService {

	@Autowired
	private OrdObservacionRepository ordObservacionRepository;
	
	@Override
	public OrdObservacion save(OrdObservacion newOrdObservacion) {
		return ordObservacionRepository.save(newOrdObservacion);
	}

}
