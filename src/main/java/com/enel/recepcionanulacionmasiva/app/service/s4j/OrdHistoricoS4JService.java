package com.enel.recepcionanulacionmasiva.app.service.s4j;

import com.enel.recepcionanulacionmasiva.app.entity.s4j.OrdHistoricoS4J;

public interface OrdHistoricoS4JService {

	OrdHistoricoS4J save(OrdHistoricoS4J newOrdHistoricoS4J);

}
