package com.enel.recepcionanulacionmasiva.app.service.s4j;

import com.enel.recepcionanulacionmasiva.app.entity.s4j.FwkAuditeventS4J;

public interface FwkAuditeventS4JService {

	FwkAuditeventS4J save(FwkAuditeventS4J newFwkAuditeventS4J);

	Long obtenerIdAuditevent();

}
