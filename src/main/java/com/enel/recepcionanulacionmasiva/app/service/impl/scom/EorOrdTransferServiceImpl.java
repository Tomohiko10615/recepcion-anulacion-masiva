package com.enel.recepcionanulacionmasiva.app.service.impl.scom;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionanulacionmasiva.app.dto.Orden;
import com.enel.recepcionanulacionmasiva.app.repository.scom.EorOrdTransferRepository;
import com.enel.recepcionanulacionmasiva.app.service.scom.EorOrdTransferService;

@Service
public class EorOrdTransferServiceImpl implements EorOrdTransferService {

	@Autowired
	private EorOrdTransferRepository eorOrdTransferRepository;

	@Override
	public void actualizar(Integer codEstProc, Integer codEstPendAnul) {
		eorOrdTransferRepository.actualizar(codEstProc, codEstPendAnul);
	}

	@Override
	public List<Orden> obtenerOrdenes(Integer codEstPendAnul, Integer codEstProc) {
		return eorOrdTransferRepository.obtenerOrdenes(codEstPendAnul, codEstProc);
	}

	@Override
	public void actualizarConError(Integer codEstAnulErr, String errSyn, String observacion, Long idOrdTransfer) {
		eorOrdTransferRepository.actualizarConError(codEstAnulErr, errSyn, observacion, idOrdTransfer);
	}

	@Override
	public void anularOrden(Integer codEstAnul, Long idOrdTransfer) {
		eorOrdTransferRepository.anularOrden(codEstAnul, idOrdTransfer);
	}

	@Override
	public void actualizarDetalle(String errSyn, Long idOrdTransfer, Short nroAnulaciones) {
		eorOrdTransferRepository.actualizarDetalle(errSyn, idOrdTransfer, nroAnulaciones);
		
	}

}
