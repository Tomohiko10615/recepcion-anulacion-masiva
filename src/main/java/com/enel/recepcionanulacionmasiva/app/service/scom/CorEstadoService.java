package com.enel.recepcionanulacionmasiva.app.service.scom;

public interface CorEstadoService {

	String obtenerDescripcion(Long idState);

}
