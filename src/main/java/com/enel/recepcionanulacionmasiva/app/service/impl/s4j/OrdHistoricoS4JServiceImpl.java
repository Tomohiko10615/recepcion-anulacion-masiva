package com.enel.recepcionanulacionmasiva.app.service.impl.s4j;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionanulacionmasiva.app.entity.s4j.OrdHistoricoS4J;
import com.enel.recepcionanulacionmasiva.app.repository.s4j.OrdHistoricoS4JRepository;
import com.enel.recepcionanulacionmasiva.app.service.s4j.OrdHistoricoS4JService;

@Service
public class OrdHistoricoS4JServiceImpl implements OrdHistoricoS4JService {

	@Autowired
	private OrdHistoricoS4JRepository ordHistoricoS4JRepository;
	
	@Override
	public OrdHistoricoS4J save(OrdHistoricoS4J newOrdHistoricoS4J) {
		return ordHistoricoS4JRepository.save(newOrdHistoricoS4J);
	}

}
