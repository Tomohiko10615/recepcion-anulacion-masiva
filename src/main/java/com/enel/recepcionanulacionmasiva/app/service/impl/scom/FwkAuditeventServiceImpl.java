package com.enel.recepcionanulacionmasiva.app.service.impl.scom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.enel.recepcionanulacionmasiva.app.entity.scom.FwkAuditevent;
import com.enel.recepcionanulacionmasiva.app.repository.scom.FwkAuditeventRepository;
import com.enel.recepcionanulacionmasiva.app.service.scom.FwkAuditeventService;

@Service
public class FwkAuditeventServiceImpl implements FwkAuditeventService {

	@Autowired
	private FwkAuditeventRepository fwkAuditeventRepository;
	
	@Override
	public FwkAuditevent save(FwkAuditevent newFwkAuditevent) {
		return fwkAuditeventRepository.save(newFwkAuditevent);
	}

}
