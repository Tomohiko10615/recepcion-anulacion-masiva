package com.enel.recepcionanulacionmasiva.app.service.scom;

import com.enel.recepcionanulacionmasiva.app.entity.scom.FwkAuditevent;

public interface FwkAuditeventService {

	FwkAuditevent save(FwkAuditevent newFwkAuditevent);

}
