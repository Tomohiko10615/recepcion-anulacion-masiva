package com.enel.recepcionanulacionmasiva.app.service.scom;

public interface WkfWorkflowService {

	void actualizarEstadoWorkflow(String estadoFinal, Long idWorkflow);

	void actualizarWorkflow(Long idWorkflowOrd);

}
