package com.enel.recepcionanulacionmasiva.app.service.scom;

public interface UsuarioService {

	Long obtenerIdUsuario(String username);

}
