package com.enel.recepcionanulacionmasiva.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.enel.recepcionanulacionmasiva.app.service.scom.UtilService;

import lombok.extern.slf4j.Slf4j;

@SpringBootApplication
@Slf4j
public class RecepcionAnulacionMasivaApplication implements CommandLineRunner {

	@Autowired
	private RecepcionAnulacionMasiva app;
	
	@Autowired
	private UtilService utilService;
	
	public static void main(String[] args) {
		SpringApplication.run(RecepcionAnulacionMasivaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		log.info("Validado parámetros de entrada...");
				
		if ( args.length != 2 ) {
			log.error("Uso de parámetros:\n"
					+ "<Usuario>\n"
					+ "<Nro ODT>");
			System.exit(1);
		}
		
		app.setUsername(args[0]);
		app.setNroODT(args[1]);
		
		app.setFechaActual(utilService.obtenerFechaSistema());
		
		if (!app.obtenerDatos()) {
			System.exit(1);
		}
		
		if (!app.crearArchivo()) {
			System.exit(1);
		}
		
		if (!app.ejecutarProceso()) {
			System.exit(1);
		}
		
		log.info("Cantidad Registros Procesados {}", app.getRegistrosProcesados());
		log.info("Cantidad Registros Actualizados {}", app.getRegistrosActualizados());
		log.info("Cantidad Registros Rechazado {}", app.getRegistrosRechazados());
		log.info("Fin: {}", utilService.obtenerFechaSistema());
		log.info("Terminó OK");
		
		app.getArchivoWriter().close();
		
		System.exit(0);
		
	}

}
