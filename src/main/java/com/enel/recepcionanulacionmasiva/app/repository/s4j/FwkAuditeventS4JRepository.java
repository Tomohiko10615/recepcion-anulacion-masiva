package com.enel.recepcionanulacionmasiva.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionanulacionmasiva.app.entity.s4j.FwkAuditeventS4J;

@Repository
public interface FwkAuditeventS4JRepository extends JpaRepository<FwkAuditeventS4J, Long> {

	@Query(value = "SELECT sqauditevent.NEXTVAL FROM DUAL", nativeQuery = true)
	Long obtenerIdAuditevent();
}
