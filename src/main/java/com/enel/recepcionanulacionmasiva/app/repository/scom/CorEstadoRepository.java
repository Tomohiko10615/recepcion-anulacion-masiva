package com.enel.recepcionanulacionmasiva.app.repository.scom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionanulacionmasiva.app.entity.scom.CorEstado;
import com.enel.recepcionanulacionmasiva.app.util.Constantes;

@Repository
public interface CorEstadoRepository extends JpaRepository<CorEstado, Long> {

	@Query(value = "SELECT CE2.DESCRIPCION "
			+ "From " + Constantes.ESQUEMADOT + "COR_ESTADO ce1, "
			+ Constantes.ESQUEMADOT + "cor_accion ca, "
			+ Constantes.ESQUEMADOT + "Cor_est_srv_electrico cose, "
			+ Constantes.ESQUEMADOT + "cor_estado ce2 "
			+ "Where cose.ID_EST_INICIAL = ce1.ID "
			+ "And cose.ID_EST_FINAL = ce2.ID "
			+ "And cose.ID_ACCION = ca.ID "
			+ "And cose.ESTADO = 'A' "
			+ "And ce1.ESTADO = 'A' "
			+ "And ce2.ESTADO = 'A' "
			+ "And ca.ESTADO = 'A' "
			+ "And ca.DESCRIPCION = 'AnularOrdenORMDesmantelamiento' "
			+ "AND CE1.DESCRIPCION = ?1", nativeQuery = true)
	String obtenerDescripcion(Long idState);

}
