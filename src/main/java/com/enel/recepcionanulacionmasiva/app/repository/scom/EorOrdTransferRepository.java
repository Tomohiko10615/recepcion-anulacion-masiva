package com.enel.recepcionanulacionmasiva.app.repository.scom;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enel.recepcionanulacionmasiva.app.dto.Orden;
import com.enel.recepcionanulacionmasiva.app.entity.scom.EorOrdTransfer;
import com.enel.recepcionanulacionmasiva.app.util.Constantes;

@Repository
@Transactional
public interface EorOrdTransferRepository extends JpaRepository<EorOrdTransfer, Long> {

	@Modifying
	@Query(value="UPDATE " + Constantes.ESQUEMADOT + "EOR_ORD_TRANSFER "
			+ "SET COD_ESTADO_ORDEN_ANT = COD_ESTADO_ORDEN, "
			+ "COD_ESTADO_ORDEN = ?1 "
			+ "WHERE COD_ESTADO_ORDEN = ?2 "
			+ "AND COD_TIPO_ORDEN_EORDER IN ('NCX.03','NCX.05')", nativeQuery=true)
	void actualizar(Integer codEstProc, Integer codEstPendAnul);

	@Query(value = "SELECT ID_ORD_TRANSFER AS idOrdTransfer, NRO_ORDEN_LEGACY AS nroOrdenLegacy, "
			+ "(CASE WHEN (SELECT cod_tipo_orden_LEGACY from ord_orden "
			+ "left join EOR_ord_transfer ON EOR_ORD_TRANSFER.nRO_orden_LEGACY = ORD_ORDEN.nRO_orden "
			+ "where EOR_ORD_TRANSFER.nRO_orden_LEGACY = EOT.nRO_orden_LEGACY)='MANT' THEN 'SCOM' ELSE 'S4J' END) as origen, "
			+ "COD_TIPO_ORDEN_LEGACY as codTipoOrdenLegacy, "
			+ "COD_TIPO_ORDEN_EORDER AS codTipoOrdenEOrder, NRO_ANULACIONES AS nroAnulaciones "
			+ "FROM " + Constantes.ESQUEMADOT + "EOR_ORD_TRANSFER EOT "
			+ "WHERE EOT.COD_ESTADO_ORDEN_ANT = ?1 "
			+ "AND EOT.COD_ESTADO_ORDEN = ?2 "
			+ "and EOT.COD_TIPO_ORDEN_EORDER IN ('NCX.03','NCX.05')", nativeQuery = true)
	List<Orden> obtenerOrdenes(Integer codEstPendAnul, Integer codEstProc);

	@Modifying
	@Query(value="UPDATE " + Constantes.ESQUEMADOT + "EOR_ORD_TRANSFER "
			+ "SET COD_ESTADO_ORDEN_ANT = COD_ESTADO_ORDEN, "
			+ "COD_ESTADO_ORDEN = ?1, "
			+ "COD_OPERACION = ?2, "
			+ "OBSERVACIONES = ?3, "
			+ "FEC_OPERACION = NOW() "
			+ "WHERE ID_ORD_TRANSFER = ?4", nativeQuery=true)
	void actualizarConError(Integer codEstAnulErr, String errSyn, String observacion, Long idOrdTransfer);

	@Modifying
	@Query(value="UPDATE " + Constantes.ESQUEMADOT + "EOR_ORD_TRANSFER "
			+ "SET COD_ESTADO_ORDEN_ANT = COD_ESTADO_ORDEN, "
			+ "COD_ESTADO_ORDEN = ?1, "
			+ "OBSERVACIONES = 'ACK Acuse de Recibo' "
			+ "WHERE ID_ORD_TRANSFER = ?2", nativeQuery=true)
	void anularOrden(Integer codEstAnul, Long idOrdTransfer);

	@Modifying
	@Query(value="UPDATE " + Constantes.ESQUEMADOT + "eor_ord_transfer_det "
			+ "SET COD_ERROR = ?1 "
			+ "WHERE id_ord_transfer = ?2 "
			+ "and accion = 'ANULACION' "
			+ "and nro_evento = ?3", nativeQuery=true)
	void actualizarDetalle(String errSyn, Long idOrdTransfer, Short nroAnulaciones);

}
