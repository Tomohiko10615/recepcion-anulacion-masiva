package com.enel.recepcionanulacionmasiva.app.repository.scom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enel.recepcionanulacionmasiva.app.entity.scom.WkfWorkflow;
import com.enel.recepcionanulacionmasiva.app.util.Constantes;

@Repository
@Transactional
public interface WkfWorkflowRepository extends JpaRepository<WkfWorkflow, Long> {

	@Modifying
	@Query(value="UPDATE " + Constantes.ESQUEMADOT + "WKF_WORKFLOW "
			+ "SET ID_OLD_STATE = ID_STATE ,ID_STATE = ?1 "
			+ "WHERE ID  = ?2", nativeQuery=true)
	void actualizarEstadoWorkflow(String estadoFinal, Long idWorkflow);

	@Modifying
	@Query(value="UPDATE " + Constantes.ESQUEMADOT + "WKF_WORKFLOW "
			+ "SET ID_OLD_STATE = ID_STATE, "
			+ "ID_STATE = 'Anulada' "
			+ "WHERE ID = ?1", nativeQuery=true)
	void actualizarWorkflow(Long idWorkflowOrd);

}
