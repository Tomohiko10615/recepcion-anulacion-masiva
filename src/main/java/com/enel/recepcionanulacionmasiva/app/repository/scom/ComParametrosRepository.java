package com.enel.recepcionanulacionmasiva.app.repository.scom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionanulacionmasiva.app.dto.EstadoError;
import com.enel.recepcionanulacionmasiva.app.entity.scom.ComParametros;
import com.enel.recepcionanulacionmasiva.app.util.Constantes;

@Repository
public interface ComParametrosRepository extends JpaRepository<ComParametros, Long> {

	@Query(value = "SELECT DISTINCT valor_alf "
			+ "FROM " + Constantes.ESQUEMADOT + "com_parametros "
			+ "WHERE sistema = 'EORDER' "
			+ "AND entidad = ?1 "
			+ "AND codigo = ?2 ", nativeQuery = true)
	String obtenerValorAlf(String entidad, String codigo);

	@Query(value = "SELECT DISTINCT valor_num "
			+ "FROM " + Constantes.ESQUEMADOT + "com_parametros "
			+ "WHERE sistema = 'EORDER' "
			+ "AND entidad = 'ESTADO_TRANSFERENCIA' "
			+ "AND codigo = ?1 ", nativeQuery = true)
	Integer obtenerValorNum(String codigo);
	
	@Query(value = "SELECT DISTINCT valor_alf AS errSyn, descripcion "
			+ "FROM " + Constantes.ESQUEMADOT + "com_parametros "
			+ "WHERE sistema = 'EORDER' "
			+ "AND entidad = 'ESTADO_ERRSYN' "
			+ "AND codigo = 'ASY001' ", nativeQuery = true)
	EstadoError obtenerEstadoError();

	@Query(value = "SELECT DISTINCT VALOR_ALF "
			+ "FROM " + Constantes.ESQUEMADOT + "COM_PARAMETROS "
			+ "WHERE SISTEMA = 'EORDER' "
			+ "AND ENTIDAD = 'ORM_TIP_ORD' "
			+ "AND CODIGO = 'DESM'", nativeQuery = true)
	String obtenerDetalles();

}
