package com.enel.recepcionanulacionmasiva.app.repository.scom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionanulacionmasiva.app.entity.scom.Usuario;
import com.enel.recepcionanulacionmasiva.app.util.Constantes;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	@Query(value="SELECT DISTINCT id "
			+ "FROM " + Constantes.ESQUEMADOT + "usuario "
			+ "where username = ?1", nativeQuery=true)
	Long obtenerUsuarioId(String username);
	
}
