package com.enel.recepcionanulacionmasiva.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enel.recepcionanulacionmasiva.app.entity.s4j.WkfWorkflowS4J;
import com.enel.recepcionanulacionmasiva.app.util.Constantes;

@Repository
@Transactional
public interface WkfWorkflowS4JRepository extends JpaRepository<WkfWorkflowS4J, Long> {

	@Modifying
	@Query(value="UPDATE " + Constantes.ESQUEMADOT + "WKF_WORKFLOW "
			+ "SET ID_OLD_STATE = ID_STATE ,ID_STATE = ?1 "
			+ "WHERE ID_WORKFLOW  = ?2", nativeQuery=true)
	void actualizarEstadoWorkflow(String estadoFinal, Long idWorkflow);

	@Modifying
	@Query(value="UPDATE " + Constantes.ESQUEMADOT + "WKF_WORKFLOW "
			+ "SET ID_OLD_STATE = ID_STATE, "
			+ "ID_STATE = 'Anulada' "
			+ "WHERE ID_WORKFLOW = ?1", nativeQuery=true)
	void actualizarWorkflow(Long idWorkflowOrd);

}
