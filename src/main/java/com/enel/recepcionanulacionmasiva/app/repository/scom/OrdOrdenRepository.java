package com.enel.recepcionanulacionmasiva.app.repository.scom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.enel.recepcionanulacionmasiva.app.dto.OrdenDetalles;
import com.enel.recepcionanulacionmasiva.app.dto.OrdenInfo;
import com.enel.recepcionanulacionmasiva.app.dto.WorkflowState;
import com.enel.recepcionanulacionmasiva.app.entity.scom.OrdOrden;
import com.enel.recepcionanulacionmasiva.app.util.Constantes;

@Repository
@Transactional
public interface OrdOrdenRepository extends JpaRepository<OrdOrden, Long>{

	@Query(value = "SELECT COUNT(*) "
			+ "FROM " + Constantes.ESQUEMADOT + "ord_orden ord, "
			+ Constantes.ESQUEMADOT + "wkf_workflow ww, "
			+ Constantes.ESQUEMADOT + "ord_tipo_orden tord "
			+ "WHERE ord.nro_orden = ?1 "
			+ "AND ord.id_tipo_orden = tord.id "
			+ "AND ord.id_workflow = ww.id "
			+ "AND tord.cod_tipo_orden = ?2 "
			+ "AND ww.id_state IN ('Creada', 'Emitida')", nativeQuery = true)
	int contarOrdenes(String nroOrdenLegacy, String codTipoOrdenLegacy);
	
	@Query(value = "SELECT COUNT(*) "
			+ "FROM " + Constantes.ESQUEMADOT + "ord_orden ord, "
			+ Constantes.ESQUEMADOT + "wkf_workflow ww, "
			+ Constantes.ESQUEMADOT + "ord_tipo_orden tord "
			+ "WHERE ord.nro_orden = ?1 "
			+ "AND ord.id_tipo_orden = tord.id_tipo_orden "
			+ "AND ord.id_workflow = ww.id_workflow "
			+ "AND tord.cod_tipo_orden = ?2 "
			+ "AND ww.id_state = 'Creada'", nativeQuery = true)
	int contarOrdenesS4J(String nroOrdenLegacy, String codTipoOrdenLegacy);

	@Query(value = "SELECT OO.ID_ORDEN AS idOrden, "
			+ "TRIM(OM.COD_MOTIVO ||'-'|| OT.CODE ||'-'|| OTRA.CODE) AS detalles "
			+ "FROM " + Constantes.ESQUEMADOT + "ORD_ORDEN OO, "
			+ Constantes.ESQUEMADOT + "ORM_ORDEN ORM, "
			+ Constantes.ESQUEMADOT + "ORD_MOTIVO_CREACION OM, "
			+ Constantes.ESQUEMADOT + "ORM_TEMA OT, "
			+ Constantes.ESQUEMADOT + "ORM_TRABAJO OTRA "
			+ "WHERE OO.ID_ORDEN = ORM.ID_ORDEN "
			+ "AND OM.ID_MOTIVO = OO.ID_MOTIVO "
			+ "AND OT.ID_TEMA = ORM.ID_TEMA "
			+ "AND OTRA.ID_TRABAJO = ORM.ID_TRABAJO "
			+ "AND OO.NRO_ORDEN = ?1", nativeQuery = true)
	OrdenDetalles obtenerOrdenDetalles(String nroOrdenLegacy);

	@Query(value = "SELECT WKF.ID, WKF.ID_STATE "
			+ "FROM " + Constantes.ESQUEMADOT + "ORD_ORDEN OO, "
			+ Constantes.ESQUEMADOT + "CLIENTE SE, "
			+ Constantes.ESQUEMADOT + "WKF_WORKFLOW WKF "
			+ "WHERE OO.ID = ?1 "
			+ "AND SE.ID_SERVICIO = OO.ID_SERVICIO "
			+ "AND WKF.ID = SE.ID_WORKFLOW", nativeQuery = true)
	WorkflowState obtenerWorkflowState(Long idOrden);

	@Query(value = "SELECT ord.id AS idOrden, "
			+ "ord.id_buzon AS idBuzon, "
			+ "ord.id_workflow AS idWorkflowOrd, "
			+ "ord.id_tipo_orden AS idTipoOrden "
			+ "FROM " + Constantes.ESQUEMADOT + "ord_orden ord, "
			+ Constantes.ESQUEMADOT + "wkf_workflow ww, "
			+ Constantes.ESQUEMADOT + "ord_tipo_orden tord "
			+ "WHERE ord.nro_orden = ?1 "
			+ "AND ord.id_tipo_orden = tord.id "
			+ "AND ord.id_workflow = ww.id "
			+ "AND tord.cod_tipo_orden = ?2", nativeQuery = true)
	OrdenInfo obtenerOrdenInfo(String nroOrdenLegacy, String codTipoOrdenLegacy);
	
	@Query(value = "SELECT ord.id_orden AS idOrden, "
			+ "ord.id_buzon AS idBuzon, "
			+ "ord.id_workflow AS idWorkflowOrd, "
			+ "ord.id_tipo_orden AS idTipoOrden "
			+ "FROM " + Constantes.ESQUEMADOT + "ord_orden ord, "
			+ Constantes.ESQUEMADOT + "wkf_workflow ww, "
			+ Constantes.ESQUEMADOT + "ord_tipo_orden tord "
			+ "WHERE ord.nro_orden = ?1 "
			+ "AND ord.id_tipo_orden = tord.id_tipo_orden "
			+ "AND ord.id_workflow = ww.id_workflow "
			+ "AND tord.cod_tipo_orden = ?2", nativeQuery = true)
	OrdenInfo obtenerOrdenInfoS4J(String nroOrdenLegacy, String codTipoOrdenLegacy);

	@Modifying
	@Query(value="UPDATE " + Constantes.ESQUEMADOT + "ORD_ORDEN "
			+ "SET "
			+ "FECHA_FINALIZACION = NOW(), "
			+ "LEIDO = 'S', "
			+ "FECHA_INGRESO_ESTADO_ACTUAL = NOW() "
			+ "WHERE NRO_ORDEN = ?1 "
			+ "AND ID = ?2 "
			+ "AND ID_TIPO_ORDEN = ?3", nativeQuery=true)
	void actualizarOrden(String nroOrdenLegacy, Long idOrden, Long idTipoOrden);

}
