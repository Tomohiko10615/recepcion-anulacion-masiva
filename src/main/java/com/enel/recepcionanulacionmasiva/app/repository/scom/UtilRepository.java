package com.enel.recepcionanulacionmasiva.app.repository.scom;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.enel.recepcionanulacionmasiva.app.util.Constantes;

@Repository
public class UtilRepository {

	@PersistenceContext
	private EntityManager em;

	public Date obtenerFechaSistema() {
		String sql = "select now()";
	    Query query = em.createNativeQuery(sql);
	    return (Date) query.getSingleResult();
	}

	public Long obtenerIdObservacion() {
		String sql = "SELECT NEXTVAL('"+ Constantes.ESQUEMADOT + "SQOBSERVACIONORDEN')";
	    Query query = em.createNativeQuery(sql);
	    return Long.parseLong(query.getSingleResult().toString());
	}
	
	public Long obtenerIdAuditevent() {
		String sql = "SELECT NEXTVAL('"+ Constantes.ESQUEMADOT + "sqauditevent')";
	    Query query = em.createNativeQuery(sql);
	    return Long.parseLong(query.getSingleResult().toString());
	}
}
