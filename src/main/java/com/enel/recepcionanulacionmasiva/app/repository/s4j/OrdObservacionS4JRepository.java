package com.enel.recepcionanulacionmasiva.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.enel.recepcionanulacionmasiva.app.entity.s4j.OrdObservacionS4J;

@Repository
public interface OrdObservacionS4JRepository extends JpaRepository<OrdObservacionS4J, Long> {

	@Query(value = "SELECT SQOBSERVACIONORDEN.NEXTVAL FROM DUAL", nativeQuery = true)
	Long obtenerIdObservacion();

}
