package com.enel.recepcionanulacionmasiva.app.repository.scom;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.enel.recepcionanulacionmasiva.app.entity.scom.FwkAuditevent;

@Repository
public interface FwkAuditeventRepository extends JpaRepository<FwkAuditevent, Long> {

}
