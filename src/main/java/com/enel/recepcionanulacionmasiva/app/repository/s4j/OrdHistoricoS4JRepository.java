package com.enel.recepcionanulacionmasiva.app.repository.s4j;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.enel.recepcionanulacionmasiva.app.entity.s4j.OrdHistoricoS4J;

@Repository
public interface OrdHistoricoS4JRepository extends JpaRepository<OrdHistoricoS4J, Long> {

}
